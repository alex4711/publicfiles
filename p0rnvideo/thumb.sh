#!/bin/bash
## find original script here: https://pastebin.com/LNeKfnUX
##
## script has to be called with bash .. NOT sh ...

desiredsnippets=20
start_time_s=20
WIDTH_VAL=320 #default thumbnail width. Can be overwritten by using third CLI argument
OUTPUT_CRF=20 #output quality. 30 is fairly low and results in small file sizes

SNIPPET_SAMPLE_F=10 #for 1 second of the video, extract this number of frames
SNIPPET_FPS="$SNIPPET_SAMPLE_F/1"
SNIPPET_LENGTH_FRAMES="30" #total len of 1 snippet is SNIPPET_LENGTH_FRAMES/SNIPPET_SAMPLE_F

THUMB_FPS=24 # used for unifying framerate before interesting moment detection
THUMB_PERCENT="0.3" # percentage length of the input video that the thumbnail should be
THUMB_MAX_TIME_S="60" # a cap for the thumb l ength

TMP_DIR="/tmp"
THUMB_SEC_FILE="thumb_start_sec.txt"
THUMB_LIST_FILE="thumb_files.txt"

THUMB_SEC_FILE="${TMP_DIR}/${THUMB_SEC_FILE}"
THUMB_LIST_FILE="${TMP_DIR}/$THUMB_LIST_FILE"

ceil_div () {
  local num=$1
  local div=$2
  echo $(( (num + div - 1) / div ))
}

calc() {
  local input=$1
  echo $( awk "BEGIN {print $input}" )
}

round_num() {
  local input=$1
  echo $( echo "$input" |  awk '{print int($1+0.5)}' )
}

generate_vidthumb() {

sourcefile=$1

source_dirname=$(dirname "${sourcefile}")
source_basename_ext=$(basename "${sourcefile}")
source_basename_no_ext=${source_basename_ext%.*}
source_basename_before_underline=${source_basename_ext%_*}

destfile_jpg_folder="${source_dirname}/${source_basename_before_underline}_thumb"
destfile_jpg="${destfile_jpg_folder}/${source_basename_before_underline}"
destfile="${source_dirname}/${source_basename_before_underline}_thumb.mp4"

if [ -f $sourcefile ]
then
  echo ""
else
  echo "source file does not exists: $sourcefile"
  return 0
fi


if [ -f $destfile ]
then
  echo "destination file already exists: $destfile "
  return 0
fi

echo "Start Thumb with destination file: $destfile"

truncate -s 0 "$THUMB_SEC_FILE"

# handle vertical videos, so the longer edge matches desired WIDTH
rotation=$(ffprobe -loglevel error -select_streams v:0 -show_entries stream_tags=rotate -of default=nw=1:nk=1 -i "$sourcefile")
if [ ! -z $rotation ]; then
    echo "the video is rotated. rotation=$rotation"
    WIDTH="-1:$WIDTH_VAL"
else
    WIDTH="$WIDTH_VAL:-1"
fi


len_s=$(ffprobe "${sourcefile}" -show_format 2>&1 | sed -n 's/duration=//p' | awk '{print int($0)}')
#echo "TotalPlaytime: $len_s"

tested_interval_s=$( ceil_div "$len_s" "$desiredsnippets" )
#echo "Interval: $tested_interval_s"

interval_frames=$( awk "BEGIN {print $THUMB_FPS*$tested_interval_s}" )
interval_frames=$( round_num "$interval_frames" )

if [ -d ${destfile_jpg_folder} ]
then
  echo "Folder already exists: ${destfile_jpg_folder}"
  return 0
else
  mkdir ${destfile_jpg_folder}
fi

ts="$start_time_s"
te=$(($ts + $tested_interval_s))
c=0
while [ $ts -lt $len_s ]; do
    #echo "c=$c: ts=$ts, te=$te"
    CMD="thumb_timestamp=\$(ffmpeg -ss $ts -i \"$sourcefile\" -vframes 1  -f null /dev/null 2>&1  < /dev/null | grep -i \"parsed_thumbnail\" | sed -n 's/.*pts_time=\([0-9]*\).*/\1/p' )"
    #echo "$CMD"
    eval ${CMD}
    thumb_timestamp=$(($thumb_timestamp + $ts))
    echo "$thumb_timestamp" >> "$THUMB_SEC_FILE"

    ffmpeg -hide_banner -loglevel fatal -ss $ts -i "${sourcefile}" -vframes 1 -q:v 2 "${destfile_jpg}_thumb${c}.jpg" >/dev/null 2>&1

    # make four small screenshots
    if [ $c -eq "6" ] || [ $c -eq "9" ] || [ $c -eq "12" ] || [ $c -eq "15" ]
    then
      tmpc=$(( (($c / 3) - 2) ))
      ffmpeg -hide_banner -loglevel fatal -ss $ts -i "${sourcefile}" -vf scale=320:180 -vframes 1 -q:v 2 "${destfile_jpg}_thumb_sm${tmpc}.jpg" >/dev/null 2>&1
    fi

    ts="$te"
    te=$(($ts + $tested_interval_s))
    c=$(($c + 1))
done

chmod 777 "${destfile_jpg_folder}"

########################################################################
#### Generating image sequences for snippets

snippet_number=0
while IFS= read -r sec
do
  sec=$( round_num "$sec" )
  #echo "snippet_number=$snippet_number, thumbnail start sec=$sec"
  CMD="ffmpeg  -loglevel error -ss ${sec} -i \"${sourcefile}\" -vf fps=${SNIPPET_FPS},scale=${WIDTH} -vframes ${SNIPPET_LENGTH_FRAMES} \"${TMP_DIR}/thumb-${source_basename_no_ext}-${snippet_number}-%03d.png\" < /dev/null"
  #echo "$CMD"
  eval ${CMD}
  snippet_number=$(($snippet_number + 1))
done < "$THUMB_SEC_FILE"
ls ${TMP_DIR}/thumb-*.png | sort -g | awk '{print "file \x27"  $0"\x27" }' > ${THUMB_LIST_FILE}

########################################################################
#### Concatenating the output
CMD="ffmpeg -loglevel error -y -r ${SNIPPET_SAMPLE_F} -f concat -safe 0 -i \"$THUMB_LIST_FILE\" -c:v libx264 -crf $OUTPUT_CRF -vf \"fps=24,format=yuv420p,scale=${WIDTH},pad=ceil(iw/2)*2:ceil(ih/2)*2\" \"${destfile}\""
#echo "$CMD"
eval ${CMD}

if [ -f $destfile ]
then
  echo 'Done!  VideoThumbnail exists ' ${destfile} '!'
  chmod 644 $destfile
else
  echo "Failed generating VideoThumbnail $destfile"
fi



########################################################################
#### CleanUp
rm ${TMP_DIR}/thumb-*.png || true
rm ${THUMB_LIST_FILE} || true
rm ${THUMB_SEC_FILE} || true

}

generate_all_fromfile(){
  declare -a myfiles
  while IFS= read -r readfromfile
  do
    myfiles+=( "$readfromfile" )
  done < "/usr/share/nginx/p0rnvideo/thumbnails.txt"

  for myfile in "${myfiles[@]}"
  do 
    echo "Reading $myfile"
    generate_vidthumb $myfile
  done
}

generate_all_fromfile